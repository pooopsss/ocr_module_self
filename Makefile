CC	:= g++
CFLAGS	:= -I/usr/local/include/opencv -I/usr/local/include/tesseract -L/usr/local/lib
OBJECTS :=
LIBRARIES	:= -lopencv_core -lopencv_imgproc -lopencv_highgui -ltesseract -fopenmp
.PHONY: all clean
all: test
test:
	$(CC) $(CFLAGS) -o ocr ocr.cpp EnOCR.cpp $(LIBRARIES)
clean:
	rm -f *.o
