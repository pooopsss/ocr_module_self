/**
 * ocr.cpp:
 * Read the digits from a scratchcard. See the tutorial at
 * http://opencv-code.com/tutorials/how-to-read-the-digits-from-a-scratchcard
 *
 * Compile with:
 * g++ -I/usr/local/include -L/usr/local/lib ocr.cpp -o ocr \
 *       -lopencv_core -lopencv_imgproc -lopencv_highgui -ltesseract
 */
#include <vector>
#include <opencv2/core/core.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <tesseract/baseapi.h>
#include <iostream>
#include "testfunc.h"
using namespace cv;
/// Global variables
Mat threshImg, erosion_dst, dilation_dst;

int erosion_elem = 0;
int erosion_size = 0;
int dilation_elem = 0;
int dilation_size = 0;
int const max_elem = 2;
int const max_kernel_size = 21;
// Covert RGB to CMYK using the formula from
// http://rapidtables.com/convert/color/rgb-to-cmyk.htm
void rgb2cmyk(cv::Mat& src, std::vector<cv::Mat>& cmyk)
{
    CV_Assert(src.type() == CV_8UC3);

    cmyk.clear();
    for (int i = 0; i < 4; ++i)
        cmyk.push_back(cv::Mat(src.size(), CV_32F));

    for (int i = 0; i < src.rows; ++i)
    {
        for (int j = 0; j < src.cols; ++j)
        {
            cv::Vec3b p = src.at<cv::Vec3b>(i,j);

            float r = p[2] / 255.;
            float g = p[1] / 255.;
            float b = p[0] / 255.;
            float k = (1 - std::max(std::max(r,g),b));

            cmyk[0].at<float>(i,j) = (1 - r - k) / (1 - k); 
            cmyk[1].at<float>(i,j) = (1 - g - k) / (1 - k);
            cmyk[2].at<float>(i,j) = (1 - b - k) / (1 - k);
            cmyk[3].at<float>(i,j) = k;
        }
    }
}

/** Function Headers */
void Erosion( int, void* );
void Dilation( int, void* );



int main()
{
    cv::Mat im0 = cv::imread("29488.jpg", CV_LOAD_IMAGE_GRAYSCALE);//29488.jpg
    if (!im0.data)
        return -1;
    namedWindow( "Erosion Demo", CV_WINDOW_AUTOSIZE );
  namedWindow( "Dilation Demo", CV_WINDOW_AUTOSIZE );
    //cv::Mat dst = im0.clone();
    int x = 850;//858
    int y = 460;//460
    int width = 350;
    int height = 80;
    cv::Mat imageROI;
    imageROI= im0(cv::Rect(x,y,width,height));
    
    

    /*cv::Mat threshImg;*/
    cv::threshold(imageROI, threshImg, 150, 255, cv::THRESH_TRUNC);
    cv::imwrite("threshold.jpg", threshImg);

   
cv::Mat copyImage = threshImg.clone();
std::vector<std::vector<cv::Point> > contours;
cv::findContours(copyImage, contours, CV_RETR_EXTERNAL, CV_CHAIN_APPROX_SIMPLE, Point(0,0));
cv::Scalar color = cv::Scalar(255,255,255);
//cv::drawContours(threshImg, contours, 20, cv::Scalar(255), -1);
/*threshImg.setTo(cv::Scalar(0));
for(int i=0;i<contours.size();i++)
{
    int size=cv::contourArea(contours[i]);
    
 
        drawContours(threshImg,contours,i,cv::Scalar(255,0,255));     
    

}*/
    
    /*cv::imwrite("xxxxx.jpg", imageROI);*/


   // std::vector<cv::Mat> cmyk;
   // rgb2cmyk(imageROI, cmyk);

    //cv::cvtColor(imageROI, imageGray, CV_RGB2HSV);
   // cv::cvtColor(imageROI, imageGray, CV_RGB2GRAY);
    //imageGray.convertTo(imageGray, CV_8U);

    //cv::cvtColor(imageROI, imageGray, cv::COLOR_BGR2GRAY) ;


    /*std::vector<std::vector<cv::Point> > contours;
     cv::findContours(imageGray, contours, CV_RETR_EXTERNAL, CV_CHAIN_APPROX_NONE);
     double max_area = 0;
    int max_idx = 0;
     for (int i = 0; i < contours.size(); i++)
    {
        double area = cv::contourArea(contours[i]);
        max_idx  = area > max_area ? i : max_idx;
        max_area = area > max_area ? area : max_area;
    }*/
    
    
    //cv::Scalar color = cv::Scalar(255,255,255);
    //imageGray.setTo(cv::Scalar(0));
    //cv::drawContours(imageGray, contours, max_idx, color,1, 8);
    
    //cv::Mat imageHSV;
    //cv::cvtColor(imageGray, imageHSV, CV_RGB2HSV);
    
   // std::vector<std::vector<cv::Point> > contours;
    //imageGray.convertTo(imageGray,)
/// Create Erosion Trackbar
  createTrackbar( "Element:\n 0: Rect \n 1: Cross \n 2: Ellipse", "Erosion Demo",
                  &erosion_elem, max_elem,
                  Erosion );

  createTrackbar( "Kernel size:\n 2n +1", "Erosion Demo",
                  &erosion_size, max_kernel_size,
                  Erosion );

  /// Create Dilation Trackbar
  createTrackbar( "Element:\n 0: Rect \n 1: Cross \n 2: Ellipse", "Dilation Demo",
                  &dilation_elem, max_elem,
                  Dilation );

  createTrackbar( "Kernel size:\n 2n +1", "Dilation Demo",
                  &dilation_size, max_kernel_size,
                  Dilation );

  /// Default start
  Erosion( 0, 0 );
  Dilation( 0, 0 );


cv::imwrite("xxxxx.jpg", threshImg);
    

    //cv::Mat im1;
    //im1 = cmyk[3].mul(1 - cmyk[1]) > 0.25;

    //cv::Mat im2;
    //im1.convertTo(im2, CV_8U);
    //im1.convertTo(im2, CV_16U);

   /* std::vector<std::vector<cv::Point> > contours;
    cv::findContours(im2, contours, CV_RETR_EXTERNAL, CV_CHAIN_APPROX_NONE);

    double max_area = 0;
    int max_idx = 0;
    for (int i = 0; i < contours.size(); i++)
    {
        double area = cv::contourArea(contours[i]);
        max_idx  = area > max_area ? i : max_idx;
        max_area = area > max_area ? area : max_area;
    }

    im2.setTo(cv::Scalar(0));
    cv::drawContours(im2, contours, max_idx, cv::Scalar(255), -1);

    cv::Mat im3= im2.clone();
    cv::cvtColor(imageROI, im3, CV_RGB2GRAY);

    im3 = ((255 - im3) & im2) > 200;
    */
    
    //cv::imwrite("xxxxx.jpg", im3);

    //cv::Mat dst = imageROI.clone();
    /*cv::findContours(dst.clone(), contours, CV_RETR_EXTERNAL, CV_CHAIN_APPROX_NONE);
    for (int i = 0; i < contours.size(); i++)
    {
        if (cv::contourArea(contours[i]) < 100)
            cv::drawContours(dst, contours, i, cv::Scalar(0), -1);
    }*/
    
   // cv::imwrite("xxxxx.jpg", dst);

    tesseract::TessBaseAPI tess;
    tess.Init(NULL, "eng", tesseract::OEM_TESSERACT_ONLY);
    tess.SetVariable("tessedit_char_whitelist", "0123456789");
    tess.SetPageSegMode(tesseract::PSM_SINGLE_LINE);
    tess.SetImage((uchar*)threshImg.data, threshImg.cols, threshImg.rows, 1, threshImg.cols);
    //tess.Recognize(0);
    char* out = tess.GetBoxText(0);//tess.GetUTF8Text();
    std::cout << out << std::endl;

   // cv::imshow("src", im0);
   // cv::imshow("dst", threshImg);
	 piska();
    cv::waitKey();
   
    return 0;
}

/**  @function Erosion  */
void Erosion( int, void* )
{
  int erosion_type;
  if( erosion_elem == 0 ){ erosion_type = MORPH_RECT; }
  else if( erosion_elem == 1 ){ erosion_type = MORPH_CROSS; }
  else if( erosion_elem == 2) { erosion_type = MORPH_ELLIPSE; }

  Mat element = getStructuringElement( erosion_type,
                                       Size( 2*erosion_size + 1, 2*erosion_size+1 ),
                                       Point( erosion_size, erosion_size ) );

  /// Apply the erosion operation
  erode( threshImg, erosion_dst, element );
  imshow( "Erosion Demo", erosion_dst );
}

/** @function Dilation */
void Dilation( int, void* )
{
  int dilation_type;
  if( dilation_elem == 0 ){ dilation_type = MORPH_RECT; }
  else if( dilation_elem == 1 ){ dilation_type = MORPH_CROSS; }
  else if( dilation_elem == 2) { dilation_type = MORPH_ELLIPSE; }

  Mat element = getStructuringElement( dilation_type,
                                       Size( 2*dilation_size + 1, 2*dilation_size+1 ),
                                       Point( dilation_size, dilation_size ) );
  /// Apply the dilation operation
  dilate( threshImg, dilation_dst, element );
  imshow( "Dilation Demo", dilation_dst );
}
