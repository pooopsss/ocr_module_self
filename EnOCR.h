#include <iostream>
#include <opencv2/core/core.hpp>
#include <omp.h>
using namespace std;


class EnOCR 
{
public:
    enum iType { IMG_SOURCE=1, IMG_CROPED=2, IMG_RELEASE=3 };
    
    
    
    void saveToFile(const string &filename, int type);
    bool isLoad(){ return isLoaded; }
    bool isReady();
    
    void SetRectToCrop(int pLeft, int pTop, int pWidth, int pHeight);
    
    char* GetTranslateDataNumeric();
    
    void Free();
    
    EnOCR(const string &filename);
    ~EnOCR();
private:
    bool loadSource(const string &filename);
    
private:
    cv::Mat imageSource;
    cv::Mat imageCroped;
    cv::Mat imageRelease;
    bool isLoaded;
    
    /*------- RECT CROP -----------*/
    int left;
    int top;
    int width;
    int height;
};