#include <sys/types.h>
#include <dirent.h>
#include <errno.h>
#include <vector>
#include <string>
#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include "EnOCR.h"
using namespace std;

#define APP_MODE_UNDEFINE 0
#define APP_MODE_FILE 1
#define APP_MODE_DIR 2

#define APP_FAILED "FAIL"
#define APP_SUCCESS "SUCCESS"

#define BUF_LENGTH 1000


string sourcePath = "";

int mode = APP_MODE_UNDEFINE;//0 - file, 1 - dir

int getFileList (string dir, vector<string> &files)
{
    DIR *dp;

    struct dirent *dirp;
    if((dp  = opendir(dir.c_str())) == NULL) {
        cout << APP_FAILED << endl;
        cout << "Error(" << errno << ") opening " << dir << endl;
        return errno;
    }

    while ((dirp = readdir(dp)) != NULL) {
        string fname = string(dirp->d_name);
        string keyContinue = "_small";
        if(fname == "." || fname == "..")
            continue;
        /*if(fname == "." || fname == "..")
            continue;*/
        std::size_t found = fname.rfind("_small");
        if (found!=std::string::npos){
            if( (fname.length()-keyContinue.length()) == found)
                continue;
        }
        
        files.push_back(string(dirp->d_name));
    }
    closedir(dp);
    return 0;
}



void Process(string &dir, string &file){
    EnOCR ocr = EnOCR(dir+"/"+file);  
    ocr.saveToFile(dir+"/"+file+"_small", (int)EnOCR::IMG_RELEASE);
    cout << file << " - " <<  ocr.GetTranslateDataNumeric() << endl;
}



vector<string> splitString(string input, string delimiter) {
    vector<string> output;
    char *pch;

    char *str = strdup(input.c_str());
    pch = strtok (str, delimiter.c_str());

    while (pch != NULL)
    {
       output.push_back(pch);
       pch = strtok (NULL,  delimiter.c_str());
    }

    free(str);
    return output;
}



int main(int argc, char *argv[])
{
    
    int nthreads, tid;
    
    //====================---------------- ������� ���������� ------------------===========================
    if(argc<3) {
        cout << APP_FAILED << endl;
        cout << "Please args" << endl;
            cout << "\tmode=file|dir" << endl;
            /*cout << "\tfile=[fullpath]" << endl;*/
            cout << "\tsource=[dirpath|filepath]" << endl;
        return 0;
    }
    
    for (int i = 0; i < argc; i++) {
        vector<string> currentParams = splitString(argv[i], "=");
        
        if(currentParams.size() == 2){
            //_____________ MODE _________________________________
            if(strcmp(currentParams[0].c_str(), "mode") == 0){
                if(strcmp(currentParams[1].c_str(), "file") == 0)
                    mode = APP_MODE_FILE;
                if(strcmp(currentParams[1].c_str(), "dir") == 0)
                    mode = APP_MODE_DIR;
            }
            /*
            //_____________ FILENAME _________________________________
            if(strcmp(currentParams[0].c_str(), "file") == 0){
                sourcePath = currentParams[1];
            }*/
            
            //_____________ SOURCE _________________________________
            if(strcmp(currentParams[0].c_str(), "source") == 0){
                sourcePath = currentParams[1];
                //char lastChar = *sourcePath.rbegin();
                char lastChar = sourcePath.at( sourcePath.length() - 1 );
                if(lastChar == '/'){
                    sourcePath.resize(sourcePath.size()-1); 
                }
            }
        }        
    }
    
    
    if(mode == APP_MODE_UNDEFINE){
        cout << APP_FAILED << endl;
        cout << "Mode not defined" << endl;
        return 0;
    }
    

    switch (mode) {
        case APP_MODE_FILE:
        {
            cout << APP_SUCCESS << endl;
            //� ������ �����
            cout << "LEN: " << 1 << endl;
            string dir = sourcePath;
            string fname = "";
            std::size_t found = sourcePath.rfind("/");
            if (found!=std::string::npos){
                dir = sourcePath.substr(0, found);
                fname = sourcePath.substr(found+1);
            }
            Process(dir, fname);
            
        } break;

        case APP_MODE_DIR:
        {
            cout << APP_SUCCESS << endl;
            //� ������ ����������
            string dir = string(sourcePath);
            vector<string> files = vector<string>();
            getFileList(dir,files);

            cout << "LEN: " << files.size() << endl;
            #pragma omp parallel for
            for (unsigned int i = 0;i < files.size();i++) {
                Process(dir, files[i]);
            }

        } break;

        default:
        {
            cout << APP_FAILED << endl;
            cout << "HZ" << endl;
            return 0;
        } break;
    }   
    
    
    return 0;
}
